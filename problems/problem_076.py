# Modify the withdraw method of BankAccount so that the bank
# account can not have a negative balance.
#
# If a person tries to withdraw more than what is in the
# balance, then the method should raise a ValueError.

from os import error
from uu import Error


class BankAccount:
    def __init__(self, balance):
        self.balance = balance

    def get_balance(self):
        if self.balance > 0:
            return self.balance
        else:
            return 0

    def withdraw(self, amount):
        # If the amount is more than what is in
        # the balance, then raise a ValueError
        self.balance -= amount
        if amount > self.balance:
            raise ValueError("Cannot withdraw that amount. Exceeded limit.") 

    def deposit(self, amount):
        self.balance += amount

account = BankAccount(-1)

print(account.get_balance())
print(account.get_balance())
