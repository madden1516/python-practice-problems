# Write a class that meets these requirements.
#
# Name:       Book
#
# Required state:
#    * author name, a string
#    * title, a string
#
# Behavior:
#    * get_author: should return "Author: «author name»"
#    * get_title:  should return "Title: «title»"
#
# Example:
#    book = Book("Natalie Zina Walschots", "Hench")

#    print(book.get_author())  # prints "Author: Natalie Zina Walschots"
#    print(book.get_title())   # prints "Title: Hench"
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.



# class Book
class Book:
# method initializer to the required state: author_name, title
    def __init__(self, author_name, title):
        # set self.author_name = author_name
        self.author_name = author_name
        # set self.title = title
        self.title = title
    
        # method get_author(self)
    def get_author(self):
        # returns self.author_name
        return "Author: " + self.author_name

    # method get_title(self)
    def get_title(self):
        # returns self.title
        return "Title: " + self.title
    

book = Book("Natalie Zina Walschots", "Hench")

print(book.get_author())  # prints "Author: Natalie Zina Walschots"
print(book.get_title())   # prints "Title: Hench"
