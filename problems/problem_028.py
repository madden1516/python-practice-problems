# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    # one way to solve it
    # letters = list(set(s))
    # return letters
    # second way to solve it
    letters = ""
    for letter in s:
        if letter not in letters:
            letters = letters + letter        
    return letters
print(remove_duplicate_letters("abccbad"))   
