# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

# def gear_for_day(is_workday, is_sunny):
#     if is_sunny == "Umbrella" and is_workday == "True":
#         return "You will need your umbrella for work today."
#     elif is_sunny == "Umbrella" and is_workday == "False":
#         return "Hurricane coming in today, be safe do not come into work today."
#     elif is_sunny == "True" and is_workday == "True":
#         return "Time to go to work."
#     else:
#         return "Freedom!!!!!!!!!!"
    
# print(gear_for_day("True", "True"))

def gear_for_day(is_workday, is_sunny):
    to_bring_list = []
    if is_workday == "True" and is_sunny == "False":
        to_bring_list.append("umbrella")
        return to_bring_list
    elif is_workday == "True" and is_sunny == "True":
        to_bring_list.append("laptop")
        return to_bring_list
    else:
        to_bring_list.append("surfboard")
        return to_bring_list

print(gear_for_day("False", "False"))
